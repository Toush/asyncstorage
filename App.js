import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  TextInput,
  Button,
  View,
  AsyncStorage
} from 'react-native';

export default class SaveSimpleString extends Component {

  constructor(props) {
    super(props);
    this.state = {
        SavedData: null
    }
  }

  async getData() {
    try {
      const value = await AsyncStorage.getItem('SavedString');
      this.setState({SavedData: value});
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

  async saveData(value) {
    try {
      await AsyncStorage.setItem('SavedString', value);
    } catch (error) {
      console.log("Error saving data" + error);
    }
  }

  async resetData() {
    try {
      await AsyncStorage.removeItem('SavedString');
      const value = await AsyncStorage.getItem('SavedString');
      this.setState({SavedData: value});
    } catch (error) {
      console.log("Error resetting data" + error);
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          text is saved as you type. retrieve it or erase it with the buttons
        </Text>

        <TextInput
          style={styles.formInput}
          placeholder="Enter data you want to save!"
          onChangeText={(value) => this.saveData(value)}
          />

        <Button
          style={styles.formButton}
          onPress={this.getData.bind(this)}
          title="Get data"
          color="#858585"
          accessibilityLabel="Get data"
        />



        <Button
          style={styles.formButton}
          onPress={this.resetData.bind(this)}
          title="Reset data"
          color="#484848"
          accessibilityLabel="Reset data"
        />

        <Text style={styles.instructions}>
          Stored data is (press GET DATA to retrieve)= {this.state.SavedData || 'no data to show, have you tried pressing GET DATA?'} 
        </Text>


      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 30,
    flex: 1,
    alignItems: 'stretch',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  formInput: {
    paddingLeft: 5,
    height: 50,
    borderWidth: 1,
    borderColor: "#555555",
  },
  formButton: {
    borderWidth: 1,
    borderColor: "#555555",
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
    marginTop: 5,
  },
});

AppRegistry.registerComponent('SaveSimpleString', () => SaveSimpleString)
